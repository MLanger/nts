
#include "NtsClientSession.h"
#include "../NtpPacketInfoImpl.h"
#include "../Logger/Logger.h"
#include <vector>
#include "./TlsClient.h"
#include "../NtsSettings.h"

NtsClientSession::NtsClientSession(NtsSettings const& settings, std::string const& keServerIp)
	:
	m_keServerIp(keServerIp),
	m_settings(settings),
	m_ntsTlsKe(m_clientState),
	m_ntpProtocol(settings, m_clientState)
{
	m_clientState.supportedNextProt = settings.client.supportedNextProtocols;
	m_clientState.supportedAeadAlgos = settings.client.supportedAeadAlgos;	
}


std::string const & NtsClientSession::getKeServerIp() const
{
	return m_keServerIp;
}



void NtsClientSession::startTlsKe(std::string const& serverIp, int port)
{
	if (serverIp == "")
	{
		LOG_ERROR("NtsClientSession::startTlsKe(): serverIP is empty");
	}

	try
	{
		boost::asio::io_service io_service;
		boost::asio::ip::tcp::resolver resolver(io_service);
		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(serverIp, std::to_string(port));
		boost::asio::ssl::context ctx(boost::asio::ssl::context::tls_client);

		ctx.load_verify_file(m_settings.client.rootCaBundleFile);

		if (m_settings.client.allowTls12 == true)
		{
			ctx.set_options(boost::asio::ssl::context::default_workarounds
				| boost::asio::ssl::context::no_sslv2
				| boost::asio::ssl::context::no_sslv3
				| boost::asio::ssl::context::no_tlsv1
				| boost::asio::ssl::context::no_tlsv1_1
				| boost::asio::ssl::context::single_dh_use);
		}
		else
		{
			ctx.set_options(boost::asio::ssl::context::default_workarounds
				| boost::asio::ssl::context::no_sslv2
				| boost::asio::ssl::context::no_sslv3
				| boost::asio::ssl::context::no_tlsv1
				| boost::asio::ssl::context::no_tlsv1_1
				| boost::asio::ssl::context::no_tlsv1_2
				| boost::asio::ssl::context::single_dh_use);
		}

		TlsClient client(io_service, ctx, iterator, m_ntsTlsKe);
		io_service.run_for(boost::asio::chrono::seconds(m_settings.client.tlsTimeoutSeconds));
	}
	catch (std::exception &err)
	{
		LOG_FATAL("NtsClientSession::startTlsKe(): Exception: Can not resolve server address ({}:{}) --> MSG: {}", serverIp, port, err.what());
	}
	return;
}


void NtsClientSession::processTlsResponsePostProcess()
{
	if (m_clientState.negotiationState == NegotiationState::uninitialized)
	{
		LOG_ERROR("NtsClientSession::processTlsResponse: TLS KE failed");
		return;
	}

	// Next Protocol: search for "NTPv4"
	const std::vector<NextProtocol> &negNextProt = m_clientState.negNextProt;
	bool foundProt = false;
	for (auto prot : negNextProt)
	{
		if (prot == NextProtocol::NTPv4)
		{
			foundProt = true;
			break;
		}
	}
	if (foundProt == false)
	{
		LOG_ERROR("NtsClientSession::processTlsResponse: Next Protocol 'NTPv4' not found");
		m_clientState.clearNegotiationState();
		return;
	}

	// success
	m_clientState.negotiationState = NegotiationState::finished;
}





void NtsClientSession::processAndVerify(NtpPacketInfoImpl & ntpPacketInfo)
{
	LOG_TRACE("NtsClientSession: procNtpRes: Session ID (time server): {}", ntpPacketInfo.getNtpTimeServerIp());
	LOG_TRACE("NtsClientSession: procNtpRes: cookies available (amount): {}", m_clientState.cookieStack.size());

	if (m_clientState.negotiationState == NegotiationState::uninitialized)
	{
		LOG_ERROR("NtsClientSession::processNtpResponse(): TLS KE noch nicht erfolgt bzw. fehlgeschlagen");
		ntpPacketInfo.setErrorCode(-1);
		return;
	}

	m_ntpProtocol.processAndVerify(ntpPacketInfo);
}

void NtsClientSession::createRequest(NtpPacketInfoImpl &ntpPacketInfo, int stage)
{
	LOG_TRACE("NtsClientSession: genNtpReq: KE server IP: {}", ntpPacketInfo.getNtsKeServerIp());
	LOG_TRACE("NtsClientSession: genNtpReq: time server IP (session ID): {}", ntpPacketInfo.getNtpTimeServerIp());
	LOG_TRACE("NtsClientSession: genNtpReq: time server Port: {}", ntpPacketInfo.getNtpTimeServerPort());
	LOG_TRACE("NtsClientSession: genNtpReq: cookies available (amount): {}", m_clientState.cookieStack.size());

	if (m_clientState.cookieStack.size() == 0)
	{
		m_clientState.clearNegotiationState();
	}

	if (m_clientState.negotiationState == NegotiationState::uninitialized)
	{
		LOG_TRACE("NtsClientSession: genNtpReq: starte NTS KE (TLS):  {}:{}", ntpPacketInfo.getNtsKeServerIp(), m_settings.server.defaultKeServerTcpPort);
		startTlsKe(ntpPacketInfo.getNtsKeServerIp(), m_settings.server.defaultKeServerTcpPort);
		processTlsResponsePostProcess();
	}

	if (m_clientState.negotiationState == NegotiationState::uninitialized)
	{
		LOG_ERROR("NtsClientSession::generateNtpRequest(): TLS KE nicht erfolgt bzw. fehlgeschlagen");
		ntpPacketInfo.setErrorCode(-1);
		return;
	}
	else
	{
		// TODO: time server IP k�nnte sich ge�ndert haben  --> Auswahl der Client Session anpassen?
		m_ntpProtocol.createRequest(ntpPacketInfo, stage);
	}
}

