/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <vector>

struct NtpPacketInfoImpl;
class NtpPacket;
class NtpExtensionField;
struct NtsSettings;
class ClientState;


class NtsClientNtp
{
public:
	NtsClientNtp() = delete;
	NtsClientNtp(NtsSettings const& settings, ClientState& clientState);
	~NtsClientNtp() = default;

	bool processAndVerify(NtpPacketInfoImpl& ntpPacketInfo);
	void createRequest(NtpPacketInfoImpl &ntpPacketInfo, int stage);

	bool verifyNtpMessage(NtpPacket &ntpPacket, NtpExtensionField const &aeadExtField); // als separate funktion?   umbenennen integrityCheck?

private:
	bool createRequest_init(NtpPacketInfoImpl &ntpPacketInfo); // create and prepare EFs and calc memory usage
	bool createRequest_update(NtpPacketInfoImpl &ntpPacketInfo); // write EFs (without AEAD)
	bool createRequest_final(NtpPacketInfoImpl &ntpPacketInfo); // securing NTP packet

	NtsSettings const & m_settings;
	ClientState & m_clientState;
};

