#include "NtsClientKe.h"
#include "../NtsConfig.h"
#include <openssl/ssl.h>

#include "ClientState.h"
#include <sstream>
#include "../Logger/Logger.h"
#include "../Records/Record.h"
#include "../Records/RecordStack.h"
#include "../Records/RecType_NextProtNeg.h"
#include "../Records/RecType_AeadAlgoNeg.h"
#include "../Records/RecType_Ntpv4ServerNeg.h"
#include "../Records/RecType_Ntpv4PortNeg.h"
#include "../Records/RecType_EndOfMessage.h"
#include "../Records/RecStackVerification.h"
#include "../cryptoFunctions.h"
#include "../generalFunctions.h"


/*
	#include "../Records/Record.h"
	#include "../Records/RecType_Error.h"
	#include "../Records/RecType_Warning.h"
	#include "../Records/RecType_NewCookie.h"
*/




NtsClientKe::NtsClientKe(ClientState& clientState)
	:
	m_clientState(clientState)
{
}



std::vector<unsigned char> NtsClientKe::generateTlsRequest()
{
	m_clientState.negotiationState = NegotiationState::uninitialized;

	// create Record: NTS Next Protocol Negotiation
	Record NtsNextProtNegRec;
	RecType::NextProtNeg::format(NtsNextProtNegRec);
	for (auto nextProtocol : m_clientState.supportedNextProt)
	{
		RecType::NextProtNeg::addProtocol(NtsNextProtNegRec, nextProtocol);
		LOG_TRACE("NtsClientKe: add Next Protcol: {}", static_cast<unsigned short>(nextProtocol));
	}
	LOG_TRACE("NtsClientKe: num Next Protcols: {}", RecType::NextProtNeg::countProtocols(NtsNextProtNegRec));

	// create Record: AEAD Algorithm Negotiation
	Record AeadAlgoNegRec;
	RecType::AeadAlgoNeg::format(AeadAlgoNegRec);
	const auto test = m_clientState.supportedAeadAlgos;
	for (auto aeadAlgo : test)
	{
		RecType::AeadAlgoNeg::addAlgorithm(AeadAlgoNegRec, aeadAlgo);
	}
	LOG_TRACE("NtsClientKe: num AEAD algos: {}", RecType::AeadAlgoNeg::countAlgorithms(AeadAlgoNegRec));

	/*
	// optional: create Record: NTPv4 Server Negotiation
	Record Ntpv4ServerNegRec;
	RecType::Ntpv4ServerNeg::format(Ntpv4ServerNegRec);
	RecType::Ntpv4ServerNeg::setServer(Ntpv4ServerNegRec, "192.168.0.1");


	// optional: create Record: NTPv4 Port Negotiation
	Record Ntpv4PortNegRec;
	RecType::Ntpv4PortNeg::format(Ntpv4PortNegRec);
	RecType::Ntpv4PortNeg::setPort(Ntpv4PortNegRec, 225);
	*/

	// create Record: End of Message
	Record EndOfMsgRec;
	RecType::EndOfMessage::format(EndOfMsgRec);


	// generate TLS ApplicationData payload
	RecordStack recStack;
	recStack.addRecord(NtsNextProtNegRec);
	recStack.addRecord(AeadAlgoNegRec);
	//recStack.addRecord(Ntpv4ServerNegRec);
	//recStack.addRecord(Ntpv4PortNegRec);
	recStack.addRecord(EndOfMsgRec);
	std::vector<unsigned char> TlsAppDataPayload = recStack.getSerializedData();

	LOG_TRACE("NtsClientKe: TLS payload size: {}", TlsAppDataPayload.size());

	return TlsAppDataPayload;
}




bool NtsClientKe::processTlsResponse(std::vector<unsigned char> const & tlsAppDataPayload, SSL &ssl)
{
	m_clientState.negotiationState = NegotiationState::uninitialized;

	// seperate records from stream
	RecordStack recStack(tlsAppDataPayload);


	// check record stack (is it NTS conform?)
	if (RecStackVerification().serverResponseStackIsValid(recStack, true) == false)
	{
		LOG_ERROR("NtsClientKe: invalid TLS data");
		return false;
	}


	//  fehler vorhanden?
	if (isErrorRecPresent(recStack) == true)
	{
		LOG_ERROR("NtsClientKe: Error records found");
		return false;
	}


	//  warnungen vorhanden?
	if (isWarningRecPresent(recStack) == true)
	{
		LOG_ERROR("NtsClientKe: Warning records found");
		return false;
	}


	// Next Protocols extrahieren und pr�fen
	std::vector<NextProtocol> negotiatedProtocols = getNegotiatedNextProtList(recStack);
	if (checkNegotiatedNextProtList(negotiatedProtocols) == false)
	{
		LOG_ERROR("NtsClientKe: Next_Protocol_Negotiation ung�ltig");
		return false;
	}
	else
	{
		m_clientState.negNextProt = negotiatedProtocols;

		// Debug output
		std::stringstream ss;
		for (auto iter : negotiatedProtocols)
		{
			ss << getNextProtocolStr(iter) << "  ";
		}
		LOG_TRACE("NtsClientKe: supported Next Protocols: {}", ss.str());
	}

	
	// AEAD-Algo pr�fen und extrahieren
	if (checkNegotiatedAeadAlgo(recStack) == false)
	{
		LOG_ERROR("NtsClientKe: AEAD negotiation failed");
		return false;
	}
	else
	{
		m_clientState.negAeadAlgo = extractNegAeadAlgo(recStack);
		LOG_TRACE("NtsClientKe: selected AEAD algorithm: {}", getAeadAlgorithmStr(m_clientState.negAeadAlgo));
	}
	
	

	// cookies extrahieren
	std::vector<std::vector<unsigned char>> cookieList = extractCookies(recStack);
	if (cookieList.size() == 0)
	{
		LOG_ERROR("NtsClientKe: keine cookies vorhanden");
		return false;
	}
	else
	{
		m_clientState.placeholderSize = static_cast<unsigned short>(cookieList[0].size());

		for (auto cookie : cookieList)
		{
			m_clientState.cookieStack.emplace_back(cookie);
		}
		
		// FOR TRACE: list cookies
		LOG_TRACE("NtsClientKe: received cookies: {}", m_clientState.cookieStack.size());
		std::stringstream CookieStackStr;
		for (auto cookie : m_clientState.cookieStack)
		{
			CookieStackStr << getItemID(cookie) << "  ";
		}
		LOG_TRACE("NtsClientKe: received cookies: {}", CookieStackStr.str());
	}


	// TLS-Schl�ssel exportieren
	m_clientState.C2SKey = extractTLSKey(ssl, TLS_Exporter_Label, NextProtocol::NTPv4, m_clientState.negAeadAlgo, TLS_C2S_KEY_ID);
	m_clientState.S2CKey = extractTLSKey(ssl, TLS_Exporter_Label, NextProtocol::NTPv4, m_clientState.negAeadAlgo, TLS_S2C_KEY_ID);

	LOG_TRACE("NtsClientKe: C2SKey:\n{}", binToHexStr(m_clientState.C2SKey, " ", 32, true));
	LOG_TRACE("NtsClientKe: S2CKey:\n{}", binToHexStr(m_clientState.S2CKey, " ", 32, true));
	
	m_clientState.negotiationState = NegotiationState::finished;

	// TLS KE beendet (tls closed?)
	return true; // KE war erfolgreich
}




bool NtsClientKe::isWarningRecPresent(RecordStack const & recStack) const
{
	if (recStack.findRecord(RecordType::Warning) != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool NtsClientKe::isErrorRecPresent(RecordStack const & recStack) const
{
	if (recStack.findRecord(RecordType::Error) != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}



// ==============================================================================================================================



bool NtsClientKe::checkNegotiatedNextProtList(std::vector<NextProtocol> const &negProtList) const
{
	if (negProtList.size() == 0)
	{
		LOG_ERROR("NtsClientKe: 'Next_Protocol_Negotiation': No supported protocols found (list empty)");
		return false;
	}
	else
	{
		for (auto protocol : negProtList)
		{
			if (isSupported(protocol) == false)
			{
				LOG_ERROR("NtsClientKe: 'Next_Protocol_Negotiation' contains unsupported protocols");
				return false;
			}
		}
	}

	return true;
}

bool NtsClientKe::checkNegotiatedAeadAlgo(RecordStack const & recStack) const
{
	// search record
	unsigned index = recStack.findRecord(RecordType::AEAD_Algorithm_Negotiation);
	if (index == 0)
	{
		LOG_ERROR("NtsClientKe: record not found: 'AEAD_Algorithm_Negotiation'");
		return false;
	}

	// extract and verify aead algo
	const Record &rec = recStack.getRecord(index);
	unsigned int numAeadAlgos = RecType::AeadAlgoNeg::countAlgorithms(rec);
	if (numAeadAlgos == 0)
	{
		LOG_ERROR("NtsClientKe: 'AEAD_Algorithm_Negotiation': no AEAD algos found");
		return false;
	}
	else if (numAeadAlgos > 1)
	{
		LOG_ERROR("NtsClientKe: 'AEAD_Algorithm_Negotiation': wrong numbers of AEAD algos (>1)");
		return false;
	}
	else
	{
		if (isSupported(RecType::AeadAlgoNeg::getAlgorithm(rec, 1)) == false)
		{
			LOG_ERROR("NtsClientKe: 'AEAD_Algorithm_Negotiation': AEAD algo not supported");
			return false;
		}
		else
		{
			return true;
		}
	}
}

AeadAlgorithm NtsClientKe::extractNegAeadAlgo(RecordStack const & recStack) const
{
	// search record
	unsigned index = recStack.findRecord(RecordType::AEAD_Algorithm_Negotiation);
	if (index == 0)
	{
		LOG_ERROR("NtsClientKe: record not found: 'AEAD_Algorithm_Negotiation'");
		return AeadAlgorithm::NOT_DEFINED;
	}
	
	const Record &rec = recStack.getRecord(index);
	return RecType::AeadAlgoNeg::getAlgorithm(rec, 1);
}



std::vector<std::vector<unsigned char>> NtsClientKe::extractCookies(RecordStack const & recStack) const
{
	std::vector<std::vector<unsigned char>> cookieList;
	unsigned int stackSize = recStack.getStackSize();

	for (unsigned int i = 1; i <= stackSize; i++)
	{
		const Record &rec = recStack.getRecord(i);
		if (rec.getType() == RecordType::New_Cookie_for_NTPv4)
		{
			cookieList.push_back(rec.getBody());
		}
	}
	return cookieList;
}



std::vector<NextProtocol> NtsClientKe::getNegotiatedNextProtList(RecordStack const & recStack) const
{
	std::vector<NextProtocol> list;

	// search record
	unsigned index = recStack.findRecord(RecordType::Next_Protocol_Negotiation);
	if (index == 0)
	{
		LOG_ERROR("NtsClientKe: record not found: 'Next_Protocol_Negotiation'");
		return list;
	}

	// extract list
	const Record &rec = recStack.getRecord(index);
	std::vector<unsigned short> nextProtNegList = RecType::NextProtNeg::getProtocolList(rec);
	for (auto prot : nextProtNegList)
	{
		list.push_back(static_cast<NextProtocol>(prot));
	}
	return list;
}



bool NtsClientKe::isSupported(NextProtocol nextProt) const
{
	for (auto prot : m_clientState.supportedNextProt)
	{
		if (prot == nextProt)
		{
			return true;
		}
	}
	return false;
}

bool NtsClientKe::isSupported(AeadAlgorithm AeadAlgo) const
{
	for (auto algo : m_clientState.supportedAeadAlgos)
	{
		if (algo == AeadAlgo)
		{
			return true;
		}
	}
	return false;
}

std::vector<unsigned char> NtsClientKe::extractTLSKey(SSL & ssl, std::string tlsExporterLabel, NextProtocol nextProt, AeadAlgorithm aeadAlgo, unsigned short mode) const
{
	unsigned short nextProtocol = static_cast<unsigned short>(nextProt);
	unsigned short aeadAlgorithm = static_cast<unsigned short>(aeadAlgo);
	unsigned int tlsKeySize = determineKeySize(aeadAlgo);
	if (tlsKeySize == 0)
	{
		LOG_ERROR("NtsServerKe::extractTLSKey(): invalid key size");
	}

	std::vector<unsigned char> context(5); // 5 bytes

	// set NextProtocol
	context.at(0) = (nextProtocol >> 8) & 0xFF;
	context.at(1) = nextProtocol & 0xFF;

	// set AeadAlgorithm
	context.at(2) = (aeadAlgorithm >> 8) & 0xFF;
	context.at(3) = aeadAlgorithm & 0xFF;

	// set Key mode
	context.at(4) = mode & 0xFF;

	// export key
	std::vector<unsigned char> tlsKey(tlsKeySize);
	int useContext = 1;
	int ret = SSL_export_keying_material(&ssl, tlsKey.data(), tlsKeySize, tlsExporterLabel.c_str(), tlsExporterLabel.length(), context.data(), context.size(), useContext); // RFC 5705

	if (ret != 1)
	{
		LOG_ERROR("NtsServerKe::extractTLSKey(): SSL_export_keying_material failed");
	}

	return tlsKey;
}
