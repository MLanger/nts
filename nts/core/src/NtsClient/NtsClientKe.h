/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <vector>
#include <string>
#include <openssl/ossl_typ.h>

class ClientState;
class RecordStack;


enum class NextProtocol : unsigned short;
enum class AeadAlgorithm : unsigned short;




class NtsClientKe
{
public:

	NtsClientKe() = delete;
	NtsClientKe(ClientState& clientState);

	std::vector<unsigned char> generateTlsRequest();
	bool processTlsResponse(std::vector<unsigned char> const& tlsAppDataPayload, SSL &ssl);



private:

	// process warning / errors
	bool isWarningRecPresent(RecordStack const & recStack) const;
	bool isErrorRecPresent(RecordStack const & recStack) const;

	// process Next Protocol
	std::vector<NextProtocol> getNegotiatedNextProtList(RecordStack const & recStack) const;
	bool checkNegotiatedNextProtList(std::vector<NextProtocol> const &negProtList) const; // ret true, wenn NTS konform

	// process AEAD Algo
	bool checkNegotiatedAeadAlgo(RecordStack const & recStack) const; // ret true, wenn NTS konform
	AeadAlgorithm extractNegAeadAlgo(RecordStack const & recStack) const;

	// process cookies
	std::vector<std::vector<unsigned char>> extractCookies(RecordStack const & recStack) const;


	bool isSupported(NextProtocol nextProt) const;
	bool isSupported(AeadAlgorithm AeadAlgo) const;

	std::vector<unsigned char> extractTLSKey(SSL &ssl, std::string tlsExporterLabel, NextProtocol nextProt, AeadAlgorithm aeadAlgo, unsigned short mode) const;
	
	ClientState & m_clientState;
};

