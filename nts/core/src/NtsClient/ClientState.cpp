#include "ClientState.h"
#include "../NtsConfig.h"


void ClientState::clearNegotiationState()
{
	negNextProt.clear();
	negAeadAlgo = AeadAlgorithm::NOT_DEFINED;
	C2SKey.clear();
	S2CKey.clear();
	cookieStack.clear();
	placeholderSize = 0;
	negotiationState = NegotiationState::uninitialized;
	clearMessageState();
}	


void ClientState::clearMessageState()
{
	uniqueIdentifier.clear();
	numPlaceholder = 0;
	uidExtField.clear();
	cookieExtField.clear();
	placeholderExtField.clear();
	authAndEncExtField.clear();
	nonce.clear();
	decryptedExtFields.clear();
	messageState = MessageState::uninitialized;
}
