#include "TlsClient.h"
#include "NtsClientKe.h"
#include "../Logger/Logger.h"
#include "../NtsConfig.h"
#include <cstddef>

TlsClient::TlsClient(boost::asio::io_service& ioService, boost::asio::ssl::context& context, boost::asio::ip::tcp::resolver::iterator endpointIterator, NtsClientKe & ntsClientKe)
	:
	m_socket(ioService, context),
	m_responseBuffer(TLS_BUFFER_SIZE),
	m_alpnList({7, 'n', 't', 's', 'k', 'e', '/', '1'}), // ALPN-Eintrag: {Stringlänge (max 255 byte) || String} --> Verkettung: ALPN-Eintrag || ALPN-Eintrag || ...
	m_ntsClientKe(ntsClientKe)
{

	m_socket.set_verify_mode (boost::asio::ssl::verify_peer); 
	m_socket.set_verify_callback([this](bool preverified, boost::asio::ssl::verify_context& ctx) {return verifyCertificate(preverified, ctx);});


	SSL_set_alpn_protos(m_socket.native_handle(), m_alpnList.data(), static_cast<unsigned int>(m_alpnList.size()));

	// Callbacks
	SSL_set_msg_callback(m_socket.native_handle(), cb_ssl_msg);		// https://www.openssl.org/docs/manmaster/man3/SSL_CTX_set_msg_callback.html
	SSL_set_msg_callback_arg(m_socket.native_handle(), this);		// set the 'arg' parameter of the SSL_CTX_set_msg_callback function


	// an connector binden
	boost::asio::async_connect(m_socket.lowest_layer(), endpointIterator, 
		[this](const boost::system::error_code& error, auto iterator) 
		{
			(void)iterator; 
			connectHandler(error); 
		}
	);

}

TlsClient::~TlsClient()
{
	LOG_DEBUG("TLS client: disconnected");
}


// https://wiki.openssl.org/index.php/Manual:SSL_CTX_set_verify(3)
bool TlsClient::verifyCertificate(bool preverified, boost::asio::ssl::verify_context& ctx) 
{
	if (preverified == false)
	{
		LOG_ERROR("certificate check failed.");
		X509* problematicCert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
		if (problematicCert == nullptr)
		{
			LOG_ERROR("failed to get certificate");
			return false;
		}
		LOG_ERROR("problematic cert: {}", X509_NAME_oneline(X509_get_subject_name(problematicCert), nullptr, 0));
		int errCode = X509_STORE_CTX_get_error(ctx.native_handle());
		std::string errStr = X509_verify_cert_error_string(errCode);
		LOG_ERROR("error {}: {}", errCode, errStr);

		return false;
	}
	return true;
}


void TlsClient::connectHandler(const boost::system::error_code& error)
{
	if (!error)
	{
		LOG_DEBUG("TLS client: connection successful");
		m_socket.async_handshake(boost::asio::ssl::stream_base::client, [this](const boost::system::error_code& ec) {handshakeHandler(ec); });
	}
	else
	{
		LOG_ERROR("TLS client: connection failed: {}", error.message());
	}
}


void TlsClient::handshakeHandler(const boost::system::error_code& error)
{
	if (!error)
	{
		LOG_DEBUG("TLS client: handshake successful");
		std::vector<unsigned char> request = m_ntsClientKe.generateTlsRequest();
		LOG_DEBUG("TLS client: request generated --> size:   {} Bytes", request.size());

		//https://www.boost.org/doc/libs/1_51_0/doc/html/boost_asio/reference/async_write/overload1.html
		boost::asio::async_write(m_socket, boost::asio::buffer(request, request.size()), 
			[this](const boost::system::error_code& ec, std::size_t bytes_transferred) {writeHandler(ec, bytes_transferred); }
		);
	}
	else
	{
		LOG_ERROR("TLS client: handshake failed: {}", error.message());
	}
}


void TlsClient::writeHandler(const boost::system::error_code& error, std::size_t bytesTransferred)
{
	if (!error)
	{
		LOG_DEBUG("TLS client: data sent successfully ({} bytes transmitted)", bytesTransferred);

		// https://www.boost.org/doc/libs/1_69_0/doc/html/boost_asio/reference/async_read/overload1.html
		boost::asio::async_read(m_socket,
								boost::asio::buffer(m_responseBuffer, m_responseBuffer.size()),  // boost::asio::dynamic_buffer(std::vector, größe(optional))
			[this](const boost::system::error_code& ec, std::size_t bytes_transferred) {readHandler(ec, bytes_transferred); }
		);

		
	}
	else
	{
		LOG_ERROR("TLS client: data transfer failed ({} bytes transmitted); err: {}", bytesTransferred, error.message());
	}
}


void TlsClient::readHandler(const boost::system::error_code& error, std::size_t bytesTransferred)
{
	LOG_DEBUG("TLS client: data received successfully ({} bytes received)", bytesTransferred);
	std::vector<unsigned char> response(m_responseBuffer.data(), m_responseBuffer.data() + bytesTransferred);
	LOG_DEBUG("TLS client: response received --> size:  {} Bytes", response.size());
	m_ntsClientKe.processTlsResponse(response, *m_socket.native_handle());

	if (!error)
	{
		boost::asio::async_read(m_socket,
		                        boost::asio::buffer(m_responseBuffer, m_responseBuffer.size()),
		                        [this](const boost::system::error_code& ec, std::size_t bytes_transferred) {
			                        readHandler(ec, bytes_transferred);
		                        });

		/*
		boost::bind(&TlsClient::readHandler, this,
											boost::asio::placeholders::error,
											boost::asio::placeholders::bytes_transferred)
		*/

	}
	else
	{
		if (error == boost::asio::error::eof)
		{
			// ok no error
		}
		else
		{
			LOG_ERROR("TLS client: async_read failed ({} bytes received); err: {}", bytesTransferred, error.message());
		}
	}
}



// https://www.openssl.org/docs/manmaster/man3/SSL_CTX_set_msg_callback.html
void TlsClient::cb_ssl_msg(int write_p, int version, int content_type, const void *buf, std::size_t len, SSL *ssl, void *arg)
{
	(void)write_p; //avoid warnings
	(void)len;
	(void)buf;
	(void)arg;

	//if (arg == nullptr)
	//{
	//	LOG_ERROR("TLS client: TlsClient::cb_ssl_msg(): arg = nullptr");
	//	return;
	//}
	//TlsClient* TlsClientInstance = static_cast<TlsClient*>(arg);

	// TODO : tls 1.3?
	if (version == TLS1_2_VERSION && content_type == SSL3_RT_HANDSHAKE)
	{
		if (SSL_get_state(ssl) == TLS_ST_CR_SRVR_DONE)
		{
			// read ALPN
			unsigned int alpnLen;
			const unsigned char *alpnData;
			std::string	selectedAlpn;
			SSL_get0_alpn_selected(ssl, &alpnData, &alpnLen);
			selectedAlpn.assign((char*) &alpnData [0], alpnLen);
			LOG_DEBUG("TLS client: selected ALPN: {}", selectedAlpn);
			/*

			if (selectedAlpn != TlsClientInstance->m_alpnList)
			{

			}
			*/
		}
	}
}





