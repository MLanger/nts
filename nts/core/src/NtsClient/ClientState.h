/*
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <vector>
#include <deque>
//#include ".././NtpPacket.h"
#include "../NtpExtensionFields/NtpExtensionField.h"
#include <cstddef>

enum class NextProtocol : unsigned short;
enum class AeadAlgorithm : unsigned short;
enum class MessageState;

enum class NegotiationState
{
	uninitialized,
	//negotiating,
	finished
};


class ClientState
{
public:

	ClientState() = default;
	~ClientState() = default;

	void clearNegotiationState(); // l�scht alles au�er:  m_supportedNextProt und m_supportedAeadAlgos
	void clearMessageState();

	NegotiationState negotiationState;
	MessageState messageState;

	// nts key exchange
	std::vector<NextProtocol> supportedNextProt;
	std::vector<AeadAlgorithm> supportedAeadAlgos;
	std::vector<NextProtocol> negNextProt;
	AeadAlgorithm negAeadAlgo;
	std::vector<unsigned char> C2SKey;
	std::vector<unsigned char> S2CKey;

	std::deque<std::vector<unsigned char>> cookieStack;
	unsigned short placeholderSize{ 0 };

	// extension fields
	std::vector<unsigned char> uniqueIdentifier; // ben�tigt?
	std::size_t numPlaceholder{ 0 };
	NtpExtensionField uidExtField;
	NtpExtensionField cookieExtField;
	NtpExtensionField placeholderExtField;
	NtpExtensionField authAndEncExtField;
	std::vector<unsigned char> nonce;
	std::vector<unsigned char> decryptedExtFields;
	// NTP server
	// NTP port
};

