#include "NtpPacket.h"
#include "Logger/Logger.h"
#include "generalFunctions.h"
#include "NtpExtensionFields/NtpExtensionField.h"

constexpr std::size_t NtpPacket::c_ntpHeader;
constexpr std::size_t NtpPacket::c_extFieldHeader;


NtpPacket::NtpPacket()
	:
	mp_ntpPacket(nullptr),
    m_ntpPacketLength(0),
	m_bufferLength(0),
	m_isValid(false)
{
}


void NtpPacket::setNtpPacket(unsigned char *ntpPacket, std::size_t packetLen, std::size_t bufferLen)
{
	if (bufferLen < packetLen)
	{
		LOG_WARN("ntp buffer size is smaller than the packet size:   buffer size: {} bytes    ntp packet len: {} bytes", bufferLen, packetLen);
	}

	mp_ntpPacket = ntpPacket;
	m_ntpPacketLength = packetLen;
	m_bufferLength = bufferLen;
}


/*
	ret true if packet is valid
*/
bool NtpPacket::rescan()
{
	m_extensionFields.clear();
	m_MAC.clear();
	m_isValid = false;
	return parseNtpPacket(mp_ntpPacket, m_ntpPacketLength);
}


unsigned char * NtpPacket::getNtpPacket() const
{
	return mp_ntpPacket;
}


std::size_t NtpPacket::getNtpPacketLength() const
{
	return m_ntpPacketLength;
}

std::size_t NtpPacket::getBufferLength() const
{
	return m_bufferLength;
}


NtpExtFieldStack & NtpPacket::getExtFieldStack()
{
	return m_extensionFields;
}

std::vector<unsigned char> const & NtpPacket::getMAC() const
{
	return m_MAC;
}


std::vector<unsigned char> NtpPacket::getSerializedPackage() const
{
	if (m_isValid == false)
	{
		LOG_ERROR("ntp packet is invalid");
		return std::vector<unsigned char>();
	}
	if (m_ntpPacketLength < 48)
	{
		LOG_ERROR("invalid ntp packet size");
		return std::vector<unsigned char>();
	}

	std::vector<unsigned char> ntp(mp_ntpPacket, mp_ntpPacket + c_ntpHeader);
	std::vector<unsigned char> extFields = m_extensionFields.getSerializedData();
	ntp.insert(ntp.end(), extFields.begin(), extFields.end());

	if (m_MAC.size() > 0)
	{
		ntp.insert(ntp.end(), m_MAC.begin(), m_MAC.end());
	}
	return ntp;
}


std::size_t NtpPacket::getFinalSize() const
{
	return getSerializedPackage().size(); // TODO expensive!
}


bool NtpPacket::overwriteNtpPacket()
{
	if (mp_ntpPacket == nullptr)
	{
		LOG_ERROR("ntp packet is invalid");
		return false;
	}

	std::vector<unsigned char> ntp = getSerializedPackage();
	LOG_TRACE("overwrite NTP packet:   new size: {}", ntp.size());
	if (m_bufferLength < ntp.size())
	{
		LOG_ERROR("ntp buffer size is too small: buffer size: {} bytes    needed: {} bytes", m_bufferLength, ntp.size());
		return false;
	}

	memcpy(mp_ntpPacket, ntp.data(), ntp.size());
	m_ntpPacketLength = ntp.size();
	return true;
}


bool NtpPacket::isValid() const
{
	return m_isValid;
}

bool NtpPacket::isMacAvailabe() const
{
	if (m_MAC.size() > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void NtpPacket::printCurrentNtp() const
{
	LOG_TRACE("current ntp packet:  ntp size: {}     data:\n{}\n", m_ntpPacketLength, binToHexStr(mp_ntpPacket, static_cast<int>(m_ntpPacketLength), " ", 48));
}

void NtpPacket::printFinalNtp() const
{
	LOG_TRACE("final ntp packet:    ntp size: {}     data:\n{}\n", getSerializedPackage().size(), binToHexStr(getSerializedPackage(), " ", 48));
}


/*
	ret true if packet is valid
*/
bool NtpPacket::parseNtpPacket(unsigned char *ntpPacket, std::size_t ntpPacketLen)
{
	if (ntpPacket == nullptr)
	{
		LOG_ERROR("ntp packet ptr is NULL");
		m_isValid = false;
		return false;
	}
	if (ntpPacketLen < c_ntpHeader)
	{
		LOG_ERROR("invalid ntp packet ({} bytes)", ntpPacketLen);
		m_isValid = false;
		return false;
	}

	// skip ntp header
	std::size_t currentPos = c_ntpHeader;
	int iter = 0;

	// find and check ExtFields/MAC
	while (currentPos < ntpPacketLen)
	{
		iter++;
		std::size_t bytesLeft = ntpPacketLen - currentPos;
		if (bytesLeft <= 24) // is a MAC
		{
			if (bytesLeft == 4) // crypto-NAK
			{
				m_MAC.assign(ntpPacket + currentPos, ntpPacket + currentPos + 4);
				currentPos += 4;
			}
			else if (bytesLeft == 20) // undefined --> 16 bytes EF + 4 bytes C-NAK  vs.   20 bytes MAC --> interpreted as MAC
			{
				LOG_WARN("ambiguous data constellation");
				m_MAC.assign(ntpPacket + currentPos, ntpPacket + currentPos + 20);
				currentPos += 20;
			}
			else if (bytesLeft == 24)
			{
				m_MAC.assign(ntpPacket + currentPos, ntpPacket + currentPos + 24);
				currentPos += 24;
			}
			else
			{
				LOG_ERROR("invalid size of the ntp packet");
				m_isValid = false;
				return false;
			}
		}
		else // min 1 EF + ggf MAC
		{
			unsigned short extFieldLength = ((*(ntpPacket + currentPos + 2) & 0xFF) << 8) | *(ntpPacket + currentPos + 3); // 2 and 3: second EF header field
			if((extFieldLength > (ntpPacketLen- currentPos)) || (extFieldLength < 4))
			{
				LOG_ERROR("invalid size of the ntp extension field: {} bytes", extFieldLength);
				m_isValid = false;
				return false;
			}
			std::vector<unsigned char> rawExtField(ntpPacket + currentPos, ntpPacket + currentPos + extFieldLength);
			NtpExtensionField extField(std::move(rawExtField)); // TODO: funktioniert std::move  oder wird copiert?
			m_extensionFields.addExtField(std::move(extField));
			currentPos += extFieldLength;
		}

		if (iter >= 100) //notfall abbruch
		{
			LOG_ERROR("iteration limit reached");
			m_isValid = false;
			return false;
		}
	}

	// post check (EF size)
	unsigned int lastEF = m_extensionFields.getStackSize();
	if (lastEF > 0)
	{
		std::size_t lastEfLen = m_extensionFields.getExtField(lastEF).getValueLength() + c_extFieldHeader;

		if ((m_MAC.size() == 0) && (lastEfLen < 28)) // min size of the last Extension Field in the absence of a MAC
		{
			LOG_ERROR("invalid size of the last ntp extension field");
			m_isValid = false;
			return false;
		}
	}
	m_isValid = true;
	return true;
}
