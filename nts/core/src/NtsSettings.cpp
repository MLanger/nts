#include "NtsSettings.h"
#include "ConfigFile.h"
#include "./Logger/Logger.h"
#include "generalFunctions.h"
#include <algorithm>  // std::transform
#include <cctype> // std::tolower


NtsSettings readConfigFile(std::string const & configFile)
{
	NtsSettings ntsSettings;
	ConfigFile cfgFile(configFile);
	std::string cfgString;
	unsigned long cfgULong;
	bool cfgBool;
	

	//	// ============================== Section: general ==============================

	// [general] -> ntsServiceMode
	cfgString.clear();
	if (cfgFile.readString(cfgString, "general", "ntsServiceMode") == false)
	{
		LOG_WARN("Failed to get 'general.ntsServiceMode' from config file ({})", configFile);
	}
	else
	{
		std::transform(cfgString.begin(), cfgString.end(), cfgString.begin(), [](char c) {return static_cast<char>(std::tolower(c)); });
		if (cfgString == "client")
		{
			ntsSettings.general.ntsServiceMode = NtsMode::client;
		}
		else if (cfgString == "server")
		{
			ntsSettings.general.ntsServiceMode = NtsMode::server;
		}
		else
		{
			LOG_WARN("invalid value: 'general.ntsServiceMode' from config file ({})", configFile);
			ntsSettings.general.ntsServiceMode = NtsMode::not_set;
		}
	}

	// ============================== Section: console_logging ======================

	// [console_logging] -> enableLogToConsole
	cfgBool = false;
	if (cfgFile.readBool(cfgBool, "console_logging", "enableLogToConsole") == false)
	{
		LOG_WARN("Failed to get 'console_logging.enableLogToConsole' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.console_logging.enableLogToConsole = cfgBool;
	}

	// [console_logging] -> logLevel
	cfgString.clear();
	if (cfgFile.readString(cfgString, "console_logging", "logLevel") == false)
	{
		LOG_WARN("Failed to get 'console_logging.logLevel' from config file ({})", configFile);
	}
	else
	{
		if (cfgString != "")
		{
			ntsSettings.console_logging.logLevel = cfgString;
		}
		else
		{
			LOG_WARN("invalid value: 'console_logging.logLevel' from config file ({})", configFile);
		}
	}

	// [console_logging] -> advancedLogInfos
	cfgBool = false;
	if (cfgFile.readBool(cfgBool, "console_logging", "advancedLogInfos") == false)
	{
		LOG_WARN("Failed to get 'console_logging.advancedLogInfos' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.console_logging.advancedLogInfos = cfgBool;
	}

	// [console_logging] -> useOutputColor
	cfgBool = false;
	if (cfgFile.readBool(cfgBool, "console_logging", "useOutputColor") == false)
	{
		LOG_WARN("Failed to get 'console_logging.useOutputColor' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.console_logging.useOutputColor = cfgBool;
	}

	// [console_logging] -> asyncLogger
	cfgBool = false;
	if (cfgFile.readBool(cfgBool, "console_logging", "asyncLogger") == false)
	{
		LOG_WARN("Failed to get 'console_logging.asyncLogger' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.console_logging.asyncLogger = cfgBool;
	}

	// ============================== Section: file_logging ======================

	// [file_logging] -> enableLogToFile
	cfgBool = false;
	if (cfgFile.readBool(cfgBool, "file_logging", "enableLogToFile") == false)
	{
		LOG_WARN("Failed to get 'file_logging.enableLogToFile' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.file_logging.enableLogToFile = cfgBool;
	}

	// [file_logging] -> logLevel
	cfgString.clear();
	if (cfgFile.readString(cfgString, "file_logging", "logLevel") == false)
	{
		LOG_WARN("Failed to get 'file_logging.logLevel' from config file ({})", configFile);
	}
	else
	{
		if (cfgString != "")
		{
			ntsSettings.file_logging.logLevel = cfgString;
		}
		else
		{
			LOG_WARN("invalid value: 'file_logging.logLevel' from config file ({})", configFile);
		}
	}

	// [file_logging] -> logFile
	cfgString.clear();
	if (cfgFile.readString(cfgString, "file_logging", "logFile") == false)
	{
		LOG_WARN("Failed to get 'file_logging.logFile' from config file ({})", configFile);
	}
	else
	{
		if (cfgString != "")
		{
			ntsSettings.file_logging.logFile = cfgString;
		}
		else
		{
			LOG_WARN("invalid value: 'file_logging.logFile' from config file ({})", configFile);
		}
	}


	// [file_logging] -> maxLogFiles
	cfgULong = 0;
	if (cfgFile.readULong(cfgULong, "file_logging", "maxLogFiles") == false)
	{
		LOG_WARN("Failed to get 'file_logging.maxLogFiles' from config file ({})", configFile);
	}
	else
	{
		if (cfgULong > 0)
		{
			ntsSettings.file_logging.maxLogFiles = cfgULong;
		}
		else
		{
			LOG_WARN("invalid value: 'file_logging.cfgULong' from config file ({})", configFile);
		}
	}

	// [file_logging] -> maxLogFileSize
	cfgULong = 0;
	if (cfgFile.readULong(cfgULong, "file_logging", "maxLogFileSize") == false)
	{
		LOG_WARN("Failed to get 'file_logging.maxLogFileSize' from config file ({})", configFile);
	}
	else
	{
		if (cfgULong > 0)
		{
			ntsSettings.file_logging.maxLogFileSize = cfgULong;
		}
		else
		{
			LOG_WARN("invalid value: 'file_logging.maxLogFileSize' from config file ({})", configFile);
		}
	}


	// [file_logging] -> dailyRotation
	cfgBool = false;
	if (cfgFile.readBool(cfgBool, "file_logging", "dailyRotation") == false)
	{
		LOG_WARN("Failed to get 'file_logging.dailyRotation' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.file_logging.dailyRotation = cfgBool;
	}


	// [file_logging] -> rotateOnStartup
	cfgBool = false;
	if (cfgFile.readBool(cfgBool, "file_logging", "rotateOnStartup") == false)
	{
		LOG_WARN("Failed to get 'file_logging.rotateOnStartup' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.file_logging.rotateOnStartup = cfgBool;
	}


	// [file_logging] -> advancedLogInfos
	cfgBool = false;
	if (cfgFile.readBool(cfgBool, "file_logging", "advancedLogInfos") == false)
	{
		LOG_WARN("Failed to get 'file_logging.advancedLogInfos' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.file_logging.advancedLogInfos = cfgBool;
	}


	// ============================== Section: server ===============================

	// [server] -> allowTls12
	cfgBool = false;
	if (cfgFile.readBool(cfgBool, "server", "allowTls12") == false)
	{
		LOG_WARN("Failed to get 'server.allowTls12' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.server.allowTls12 = cfgBool;
	}


	// [server] -> defaultKeServerTcpPort
	cfgULong = 0;
	if (cfgFile.readULong(cfgULong, "server", "defaultKeServerTcpPort") == false)
	{
		LOG_WARN("Failed to get 'server.defaultKeServerTcpPort' from config file ({})", configFile);
	}
	else
	{
		if (cfgULong <= 0xFFFF)
		{
			ntsSettings.server.defaultKeServerTcpPort = static_cast<unsigned short>(cfgULong);
		}
		else
		{
			LOG_WARN("invalid value ( >65535 ): 'server.defaultKeServerTcpPort' from config file ({})", configFile);
			ntsSettings.server.defaultKeServerTcpPort = 0;
		}
	}

	// [server] -> aeadAlgoForMasterKey
	cfgString.clear();
	if (cfgFile.readString(cfgString, "server", "aeadAlgoForMasterKey") == false)
	{
		LOG_WARN("Failed to get 'server.aeadAlgoForMasterKey' from config file ({})", configFile);
	}
	else
	{
		AeadAlgorithm aead = getAeadAlgorithmFromStr(cfgString);
		if (isSupported(aead) == true)
		{
			ntsSettings.server.aeadAlgoForMasterKey = aead;
		}
		else
		{
			LOG_WARN("algorithm not supported: 'server.aeadAlgoForMasterKey' from config file ({})", configFile);
			ntsSettings.server.aeadAlgoForMasterKey = AeadAlgorithm::NOT_DEFINED;
		}
	}

	// [server] -> mdAlgoForMasterKeyHkdf
	cfgString.clear();
	if (cfgFile.readString(cfgString, "server", "mdAlgoForMasterKeyHkdf") == false)
	{
		LOG_WARN("Failed to get 'server.mdAlgoForMasterKeyHkdf' from config file ({})", configFile);
	}
	else
	{
		MessageDigestAlgo md = getMdAlgorithmFromStr(cfgString);
		if (isSupported(md) == true)
		{
			ntsSettings.server.mdAlgoForMasterKeyHkdf = md;
		}
		else
		{
			LOG_WARN("algorithm not supported: 'server.mdAlgoForMasterKeyHkdf' from config file ({})", configFile);
			ntsSettings.server.mdAlgoForMasterKeyHkdf = MessageDigestAlgo::Not_Defined;
		}
	}

	// [server] -> keyRotationHistory
	cfgULong = 0;
	if (cfgFile.readULong(cfgULong, "server", "keyRotationHistory") == false)
	{
		LOG_WARN("Failed to get 'server.keyRotationHistory' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.server.keyRotationHistory = static_cast<unsigned short>(cfgULong);
	}

	// [server] -> keyRotationIntervalSeconds
	cfgULong = 0;
	if (cfgFile.readULong(cfgULong, "server", "keyRotationIntervalSeconds") == false)
	{
		LOG_WARN("Failed to get 'server.keyRotationIntervalSeconds' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.server.keyRotationIntervalSeconds = static_cast<unsigned short>(cfgULong);
	}

	// [server] -> serverCertChainFile
	cfgString.clear();
	if (cfgFile.readString(cfgString, "server", "serverCertChainFile") == false)
	{
		LOG_WARN("Failed to get 'server.serverCertChainFile' from config file ({})", configFile);
	}
	else
	{
		// TODO: check --> exist file?
		ntsSettings.server.serverCertChainFile = cfgString;
	}

	// [server] -> serverPrivateKey
	cfgString.clear();
	if (cfgFile.readString(cfgString, "server", "serverPrivateKey") == false)
	{
		LOG_WARN("Failed to get 'server.serverPrivateKey' from config file ({})", configFile);
	}
	else
	{
		// TODO: check --> exist file?
		ntsSettings.server.serverPrivateKey = cfgString;
	}

	// [server] -> supportedNextProtocols
	cfgString.clear();
	if (cfgFile.readString(cfgString, "server", "supportedNextProtocols") == false)
	{
		LOG_WARN("Failed to get 'server.supportedNextProtocols' from config file ({})", configFile);
	}
	else
	{
		// parse line
		std::vector<std::string> listTokens;
		parseString(cfgString, listTokens, ";, ");
		for (std::vector<std::string>::size_type i = 0; i < listTokens.size(); i++)
		{
			NextProtocol nextProtocol = getNextProtocolFromStr(listTokens.at(i));
			if (isSupported(nextProtocol) == true)
			{
				ntsSettings.server.supportedNextProtocols.push_back(nextProtocol);
			}
			else
			{
				LOG_WARN("protocol ({}) not supported: 'server.supportedNextProtocols' from config file ({})", listTokens.at(i), configFile);
			}
		}
	}

	// [server] -> supportedAeadAlgos
	cfgString.clear();
	if (cfgFile.readString(cfgString, "server", "supportedAeadAlgos") == false)
	{
		LOG_WARN("Failed to get 'server.supportedAeadAlgos' from config file ({})", configFile);
	}
	else
	{
		// parse line
		std::vector<std::string> listTokens;
		parseString(cfgString, listTokens, ";, ");
		for (std::vector<std::string>::size_type i = 0; i < listTokens.size(); i++)
		{
			AeadAlgorithm aead = getAeadAlgorithmFromStr(listTokens.at(i));
			if (isSupported(aead) == true)
			{
				ntsSettings.server.supportedAeadAlgos.push_back(aead);
			}
			else
			{
				LOG_WARN("algorithm ({}) not supported: 'server.supportedAeadAlgos' from config file ({})", listTokens.at(i), configFile);
			}
		}
	}

	// [server] -> maxAmountOfCookies
	cfgULong = 0;
	if (cfgFile.readULong(cfgULong, "server", "maxAmountOfCookies") == false)
	{
		LOG_WARN("Failed to get 'server.maxAmountOfCookies' from config file ({})", configFile);
	}
	else
	{
		if (cfgULong < 1) // TODO set max limit
		{
			cfgULong = 8;
			LOG_WARN("'server.maxAmountOfCookies' from config file ({}): invalid value (using default: 8)", configFile);
		}
		
		ntsSettings.server.maxAmountOfCookies = static_cast<unsigned short>(cfgULong);
	}

	// ============================== Section: client ===============================

	// [client] -> tlsTimeoutSeconds
	cfgULong = 0;
	if (cfgFile.readULong(cfgULong, "client", "tlsTimeoutSeconds") == false)
	{
		LOG_WARN("Failed to get 'client.tlsTimeoutSeconds' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.client.tlsTimeoutSeconds = cfgULong;
	}


	// [client] -> allowTls12
	cfgBool = false;
	if (cfgFile.readBool(cfgBool, "client", "allowTls12") == false)
	{
		LOG_WARN("Failed to get 'client.allowTls12' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.client.allowTls12 = cfgBool;
	}


	cfgString.clear();
	if (cfgFile.readString(cfgString, "client", "rootCaBundleFile") == false)
	{
		LOG_WARN("Failed to get 'client.rootCaBundleFile' from config file ({})", configFile);
	}
	else
	{
		// TODO: check --> exist file?
		ntsSettings.client.rootCaBundleFile = cfgString;
	}


	// [client] -> supportedNextProtocols
	cfgString.clear();
	if (cfgFile.readString(cfgString, "client", "supportedNextProtocols") == false)
	{
		LOG_WARN("Failed to get 'client.supportedNextProtocols' from config file ({})", configFile);
	}
	else
	{
		// parse line
		std::vector<std::string> listTokens;
		parseString(cfgString, listTokens, ";, ");
		for (std::vector<std::string>::size_type i = 0; i < listTokens.size(); i++)
		{
			NextProtocol nextProtocol = getNextProtocolFromStr(listTokens.at(i));
			if (isSupported(nextProtocol) == true)
			{
				ntsSettings.client.supportedNextProtocols.push_back(nextProtocol);
			}
			else
			{
				LOG_WARN("protocol ({}) not supported: 'client.supportedNextProtocols' from config file ({})", listTokens.at(i), configFile);
			}
		}
	}


	// [client] -> supportedAeadAlgos
	cfgString.clear();
	if (cfgFile.readString(cfgString, "client", "supportedAeadAlgos") == false)
	{
		LOG_WARN("Failed to get 'client.supportedAeadAlgos' from config file ({})", configFile);
	}
	else
	{
		// parse line
		std::vector<std::string> listTokens;
		parseString(cfgString, listTokens, ";, ");
		for (std::vector<std::string>::size_type i = 0; i < listTokens.size(); i++)
		{
			AeadAlgorithm aead = getAeadAlgorithmFromStr(listTokens.at(i));
			if (isSupported(aead) == true)
			{
				ntsSettings.client.supportedAeadAlgos.push_back(aead);
			}
			else
			{
				LOG_WARN("algorithm ({}) not supported (skipped): 'client.supportedAeadAlgos' from config file ({})", listTokens.at(i), configFile);
			}
		}
	}


	// [client] -> maxAmountOfCookies
	cfgULong = 0;
	if (cfgFile.readULong(cfgULong, "client", "maxAmountOfCookies") == false)
	{
		LOG_WARN("Failed to get 'client.maxAmountOfCookies' from config file ({})", configFile);
	}
	else
	{
		if (cfgULong < 1) // TODO set max limit
		{
			cfgULong = 8;
			LOG_WARN("'client.maxAmountOfCookies' from config file ({}): invalid value (using default: 8)", configFile);
		}

		ntsSettings.client.maxAmountOfCookies = static_cast<unsigned short>(cfgULong);
	}
	
	return ntsSettings;
}


