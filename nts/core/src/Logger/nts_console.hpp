#pragma once

#ifndef SPDLOG_H
#error "spdlog.h must be included before this file."
#endif



#include "spdlog/sinks/base_sink.h"
#include "spdlog/details/file_helper.h"
#include "spdlog/details/null_mutex.h"
#include "spdlog/fmt/fmt.h"
#include "spdlog/details/os.h"
#include "rang/rang.hpp"
#include <mutex>



namespace spdlog
{
	namespace sinks
	{
		template<typename Mutex>
		class nts_console final : public spdlog::sinks::base_sink <Mutex>
		{
		public:
			nts_console() = delete;

			nts_console(bool allowColors, bool advancedLogInfos)
				:
				m_allowColors(allowColors),
				m_advancedLogInfos(advancedLogInfos)
			{}

		protected:
			void sink_it_(const spdlog::details::log_msg& msg) override
			{
				fmt::memory_buffer formatted;
				spdlog::sinks::sink::formatter_->format(msg, formatted);
				std::string fMsg = fmt::to_string(formatted);
			
				if (fMsg.length() == 0)
				{
					return;
				}

				const char separator = ';';
				unsigned int numTokens = 3; // expected format: "%H:%M:%S;%e;%v"
				

				std::vector<std::string> tokens = getTokens(fMsg, separator, numTokens);
				if (tokens.size() != numTokens)
				{
					std::cout << fMsg;
				}
				else
				{
					// time: %H:%M:%S
					std::cout << fmt::format("{}", tokens[0]);

					// milliseconds
					if (m_advancedLogInfos == true)
					{
						std::cout << fmt::format(".{}", tokens[1]);
					}

					// loglevel
					if (m_allowColors == true)
					{
						switch (msg.level)
						{
						case spdlog::level::trace:
							std::cout << "  [" << rang::bg::reset << rang::fg::gray << "trace" << rang::bg::reset << rang::fg::reset << "]";
							break;

						case spdlog::level::debug:
							std::cout << "  [" << rang::bg::reset << rang::fg::cyan << "debug" << rang::bg::reset << rang::fg::reset << "]";
							break;

						case spdlog::level::info:
							std::cout << "  [" << rang::bg::reset << rang::fgB::cyan << "info " << rang::bg::reset << rang::fg::reset << "]";
							break;

						case spdlog::level::warn:
							std::cout << "  [" << rang::bg::reset << rang::fgB::yellow << "warn " << rang::bg::reset << rang::fg::reset << "]";
							break;

						case spdlog::level::err:
							std::cout << "  [" << rang::bg::reset << rang::fgB::red << "error" << rang::bg::reset << rang::fg::reset << "]";

							break;
						case spdlog::level::critical:
							std::cout << "  [" << rang::bg::red << rang::fgB::gray << "fatal" << rang::bg::reset << rang::fg::reset << "]";
							break;

						default:
							std::cout <<   "[" << rang::bg::red << rang::fgB::gray << " ??? " << rang::bg::reset << rang::fg::reset << "]";
						}
					}
					else
					{
						switch (msg.level)
						{
						case spdlog::level::trace:
							std::cout << "  [trace]";
							break;

						case spdlog::level::debug:
							std::cout << "  [debug]";
							break;

						case spdlog::level::info:
							std::cout << "  [info ]";
							break;

						case spdlog::level::warn:
							std::cout << "  [warn ]";
							break;

						case spdlog::level::err:
							std::cout << "  [error]";
							break;

						case spdlog::level::critical:
							std::cout << "  [fatal]";
							break;

						default:
							std::cout << "  [ ??? ]";
						}
					}

					// logger name
					if (m_allowColors == true)
					{
						std::cout << " [" << rang::bg::reset << rang::fg::green << msg.logger_name->c_str() << rang::bg::reset << rang::fg::reset << "]";
					}
					else
					{
						std::cout << " [" << msg.logger_name->c_str() << "]";
					}
					
					// location
					if (m_advancedLogInfos == true)
					{
						std::cout << fmt::format(" [{:<28}]", msg.source.filename);	 // file
						std::cout << fmt::format(" [{:>4}]", msg.source.line);	     // line
						std::cout << fmt::format(" [{:<25}]", msg.source.funcname);	 // function
						
					}

					// message
					std::cout << fmt::format("  {}", tokens[2]);
				}
			}

			void flush_() override
			{
				std::cout << std::flush;
			}

		private:


			// maxTokens: 0 = unlimited
			// \n \r werden nicht entfernt
			std::vector<std::string> getTokens(std::string const & stringToParse, const char separator, unsigned int maxTokens)
			{
				unsigned int tokenCount = 0;
				std::vector<std::string> tokens;
				auto pos = stringToParse.begin();

				while (pos < stringToParse.end())
				{
					if ((maxTokens > 0) && (tokenCount + 1 == maxTokens))
					{
						tokens.emplace_back(pos, stringToParse.end());
						break;
					}
					else
					{
						auto end = std::find(pos, stringToParse.end(), separator);
						tokens.emplace_back(pos, end);
						if (end == stringToParse.end())
						{
							break;
						}
						else
						{
							pos = end + 1;
							tokenCount++;
						}
					}
				}

				return tokens;
			}

			bool m_allowColors;
			bool m_advancedLogInfos;

		}; // class


		using nts_console_mt = nts_console<std::mutex>;
		using nts_console_st = nts_console<spdlog::details::null_mutex>;



	} // namespace sinks

	//
	// factory functions
	//
	template<typename Factory = spdlog::default_factory>
	inline std::shared_ptr<spdlog::logger> nts_console_mt(const std::string &logger_name, bool allowColors, bool advancedLogInfos)
	{
		return Factory::template create<sinks::nts_console_mt>(logger_name, allowColors, advancedLogInfos);
	}

	template<typename Factory = spdlog::default_factory>
	inline std::shared_ptr<spdlog::logger> nts_console_st(const std::string &logger_name, bool allowColors, bool advancedLogInfos)
	{
		return Factory::template create<sinks::nts_console_st>(logger_name, allowColors, advancedLogInfos);
	}

} // namespace spdlog