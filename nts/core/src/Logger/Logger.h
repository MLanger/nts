/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE

#include <spdlog/tweakme.h>
#include <spdlog/spdlog.h>

 //https://github.com/gabime/spdlog/wiki
 //http://fmtlib.net/latest/syntax.html
 //https://github.com/agauniyal/rang


#define LOG_SOURCE_LOC spdlog::source_loc{SPDLOG_FILE_BASENAME(__FILE__), __LINE__, SPDLOG_FUNCTION}
#define SHOULD_LOG(lvl) (Logger::getInstance().shouldLogToConsole(lvl) || Logger::getInstance().shouldLogToFile(lvl))

//#if _DEBUG
#define LOG_TRACE(...) if (SHOULD_LOG(spdlog::level::trace) == true) {Logger::getInstance().log(LOG_SOURCE_LOC, spdlog::level::trace, __VA_ARGS__);}
//#else
//#define LOG_TRACE(...) (void)0;
//#endif

#define LOG_DEBUG(...) if (SHOULD_LOG(spdlog::level::debug) == true) {Logger::getInstance().log(LOG_SOURCE_LOC, spdlog::level::debug, __VA_ARGS__);}
#define LOG_INFO(...)  if (SHOULD_LOG(spdlog::level::info) == true) {Logger::getInstance().log(LOG_SOURCE_LOC, spdlog::level::info, __VA_ARGS__);}
#define LOG_WARN(...)  if (SHOULD_LOG(spdlog::level::warn) == true) {Logger::getInstance().log(LOG_SOURCE_LOC, spdlog::level::warn, __VA_ARGS__);}
#define LOG_ERROR(...) if (SHOULD_LOG(spdlog::level::err) == true) {Logger::getInstance().log(LOG_SOURCE_LOC, spdlog::level::err, __VA_ARGS__);}
#define LOG_FATAL(...) if (SHOULD_LOG(spdlog::level::critical) == true) {Logger::getInstance().log(LOG_SOURCE_LOC, spdlog::level::critical, __VA_ARGS__);}

#define LOG_RAW(...)   Logger::getInstance().logRaw(__VA_ARGS__);






#if _DEBUG
#define CONDITIONAL_TRACE(X) if (SHOULD_LOG(spdlog::level::trace) == true) {X}
#else
#define CONDITIONAL_TRACE(X) (void)0;
#endif













struct NtsSettings;
class Logger
{ 
public:

	static Logger& getInstance();

	void configure(NtsSettings const& settings);

	bool shouldLogToConsole(spdlog::level::level_enum lvl);
	bool shouldLogToFile(spdlog::level::level_enum lvl);

	spdlog::level::level_enum getLevel(std::string const & lvl) const;


	template<typename... Args> 
	inline void logRaw(const char *fmt, const Args &... args)
	{
		m_consoleRawLogger->critical(fmt, args...);
	}

	template<typename... Args>
	inline void log(spdlog::source_loc source, spdlog::level::level_enum lvl, const char *fmt, const Args &... args)
	{
		if (m_consoleLogger->should_log(lvl) == true)
		{
			m_consoleLogger->log(source, lvl, fmt, args...);
		}

		if (m_fileLoggerInit == true)
		{
			if (m_fileLogger->should_log(lvl) == true)
			{
				m_fileLogger->log(source, lvl, fmt, args...);
			}
		}
	}


private:
	Logger();
	~Logger();

	Logger(Logger const&) = delete;
	void operator=(Logger const&) = delete;
	Logger(Logger const&&) = delete;
	void operator=(Logger const&&) = delete;

	std::shared_ptr<spdlog::logger> m_consoleRawLogger;
	std::shared_ptr<spdlog::logger> m_consoleLogger;
	std::shared_ptr<spdlog::logger> m_fileLogger;

	bool m_fileLoggerInit;
};
