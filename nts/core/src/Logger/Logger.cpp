#include "./Logger.h"
#include <spdlog/async.h>
#include <spdlog/sinks/stdout_sinks.h>
#include "./nts_console.hpp"
#include "./combined_rotating_logger.hpp"
#include <boost/filesystem.hpp>
#include "../NtsSettings.h"
#include <algorithm>  // std::transform
#include <cctype> // std::tolower

Logger::Logger()
	:
	m_fileLoggerInit(false)
{
	try
	{
		m_consoleLogger = spdlog::nts_console_mt("nts", true, false);
		m_consoleLogger->set_level(spdlog::level::trace);
		m_consoleLogger->set_pattern("%H:%M:%S;%e;%v");
		m_consoleLogger->flush_on(spdlog::level::trace);

		m_consoleRawLogger = spdlog::stdout_logger_mt("raw");
		m_consoleRawLogger->set_level(spdlog::level::critical);
		m_consoleRawLogger->set_pattern("%v");
		m_consoleRawLogger->flush_on(spdlog::level::critical);

		spdlog::flush_every(std::chrono::seconds(1));
		spdlog::set_default_logger(m_consoleLogger);
	}
	catch (const spdlog::spdlog_ex& ex)
	{
		std::cout << "Log initialization failed: " << ex.what() << std::endl;
	}
}


Logger::~Logger()
{
	m_consoleLogger->flush();
	m_consoleRawLogger->flush();

	if (m_fileLoggerInit == true)
	{
		m_fileLogger->flush();
	}

	spdlog::drop_all();
}



void Logger::configure(NtsSettings const & settings)
{
	if (settings.console_logging.enableLogToConsole == true)
	{
		// load settings
		std::string const& logLevel = settings.console_logging.logLevel;
		bool useOutputColor = settings.console_logging.useOutputColor;
		bool advancedLogInfos = settings.console_logging.advancedLogInfos;

		// reconfigure console logger
		spdlog::drop("nts");

		if (settings.console_logging.asyncLogger == true)
		{
			m_consoleLogger = spdlog::nts_console_mt<spdlog::async_factory>("nts", useOutputColor, advancedLogInfos);
		}
		else
		{
			m_consoleLogger = spdlog::nts_console_mt("nts", useOutputColor, advancedLogInfos);
		}
		m_consoleLogger->set_level(getLevel(logLevel));
		m_consoleLogger->set_pattern("%H:%M:%S;%e;%v");
		m_consoleLogger->flush_on(spdlog::level::warn);
		spdlog::set_default_logger(m_consoleLogger);


		// reconfigure raw console logger
		spdlog::drop("raw");

		if (settings.console_logging.asyncLogger == true)
		{
			m_consoleRawLogger = spdlog::nts_console_mt<spdlog::async_factory>("raw", useOutputColor, advancedLogInfos);
		}
		else
		{
			m_consoleRawLogger = spdlog::nts_console_mt("raw", useOutputColor, advancedLogInfos);
		}
		m_consoleRawLogger->set_level(spdlog::level::critical);
		m_consoleRawLogger->set_pattern("%v");
		m_consoleRawLogger->flush_on(spdlog::level::critical);
	}
	else
	{
		m_consoleLogger->set_level(spdlog::level::off);
	}


	if (settings.file_logging.enableLogToFile == true)
	{
		try
		{
			// load settings
			std::string const& logFile = settings.file_logging.logFile;
			std::string const& logLevel = settings.file_logging.logLevel;
			unsigned int maxLogFileSize = settings.file_logging.maxLogFileSize;
			unsigned int maxLogFiles = settings.file_logging.maxLogFiles;
			bool dailyRotation = settings.file_logging.dailyRotation;
			bool rotateOnStartup = settings.file_logging.rotateOnStartup;
			bool advancedLogInfos = settings.file_logging.advancedLogInfos;


			// check path and name of the log file
			boost::filesystem::path filePath(logFile);
			if ((filePath.filename_is_dot() == true) || (filePath.filename_is_dot_dot() == true))
			{
				throw std::runtime_error("invalid filename");
			}
			if (boost::filesystem::exists(filePath.parent_path()) == false)
			{
				if (boost::filesystem::create_directories(filePath.parent_path()) == false)
				{
					throw std::runtime_error("error creating directories");
				}
			}


			spdlog::drop("file_logger");
			m_fileLogger = spdlog::combined_rotating_logger_mt("file_logger", logFile, maxLogFileSize, maxLogFiles, rotateOnStartup, dailyRotation);
			m_fileLogger->set_level(getLevel(logLevel));

			if (advancedLogInfos == true)
			{
				m_fileLogger->set_pattern("%d.%m.%Y  %H:%M:%S.%f  [%-5l]  [%-28s]  [%4#]  [%-25!]  %v");
			}
			else
			{
				m_fileLogger->set_pattern("%d.%m.%Y  %H:%M:%S.%e  [%-5l]  %v");
			}

			m_fileLogger->flush_on(spdlog::level::warn);
			m_fileLoggerInit = true;
		}
		catch (const boost::filesystem::filesystem_error& e)
		{
			LOG_ERROR("error creating the logfile: {}", e.what());
			m_fileLoggerInit = false;
		}
		catch (const spdlog::spdlog_ex &e)
		{
			LOG_ERROR("error creating the logfile: {}", e.what());
			m_fileLoggerInit = false;
		}
		catch (std::exception &e)
		{
			LOG_ERROR("error creating the logfile: {}", e.what());
			m_fileLoggerInit = false;
		}
	}
	else
	{
		m_fileLoggerInit = false;
	}
}



Logger& Logger::getInstance()
{
	static Logger instance; // Guaranteed to be destroyed.
	return instance;
}



bool Logger::shouldLogToConsole(spdlog::level::level_enum lvl)
{
	return m_consoleLogger->should_log(lvl);
}

bool Logger::shouldLogToFile(spdlog::level::level_enum lvl)
{
	if (m_fileLoggerInit == true)
	{
		return m_fileLogger->should_log(lvl);
	}
	else
	{
		return false;
	}
}




spdlog::level::level_enum Logger::getLevel(std::string const & lvl) const
{
	std::string loglevel = lvl;
	std::transform(loglevel.begin(), loglevel.end(), loglevel.begin(), 
		[](char c) {return static_cast<char>(std::tolower(c)); });

	if (loglevel == "trace")   return spdlog::level::level_enum::trace;
	if (loglevel == "debug")   return spdlog::level::level_enum::debug;
	if (loglevel == "info")    return spdlog::level::level_enum::info;
	if (loglevel == "warning") return spdlog::level::level_enum::warn;
	if (loglevel == "error")   return spdlog::level::level_enum::err;
	if (loglevel == "fatal")   return spdlog::level::level_enum::critical;

	LOG_WARN("loglevel ungueltig");
	return spdlog::level::level_enum::info;
}