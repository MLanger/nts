/*
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <vector>
#include "NtpExtensionFields/NtpExtFieldStack.h"
#include <cstddef>
/*
   erzeugt keine Kopie und greift auf das �bergebene NTP-Paket direkt zu


   RFC 7822:
   ====================================================================
   NTP-Header:
	- 48 bytes

	MAC sizes:
		- 24 bytes
		- 20 bytes
		- 4 bytes (crypto-NAK)


	EFs:
		- All extension fields are zero-padded to a word (4 bytes)
		- min size: 16 bytes


	1 EF mit MAC:
		- EF spec muss algo definieren, dass f�r die Berechnung der MAC ben�tigt wird
		- min EF size: 16 bytes

	>1 EF mit MAC:
		- alle EFs die ein MAC ben�tigen, m�ssen den selben MAC-Algo und MAC-L�nge haben
		- min EF size: 16 bytes

	0 EF mit MAC:
		- MAC-L�nge darf max. 24 bytes sein
			- au�er wenn client und server eine l�ngere vereinbart haben

	1 EF ohne MAC:
		- min size: 28 bytes

	>1 EF ohne MAC:
		- min size: 16 bytes
		- das letzte EF muss min. 28 bytes gro� sein.


	--------------------------------------------------------------------------

	---------- MAC only

	MAC - 4 / 20 / 24


	---------- EF + MAC

	EF - 16
	MAC - 4 / 20 / 24

	---------- EFs + MAC

	EF - 16
	EF - 16
	EF - 16
	MAC - 4 / 20 / 24


	---------- EF only
	EF - 28


	---------- EFs only
	EF - 16
	EF - 16
	EF - 28

	====================================================================

*/

// TODO: in-place overwrite
// TODO: discard MAC?  

class NtpPacket
{
public:
	NtpPacket();
	~NtpPacket() = default;

	void setNtpPacket(unsigned char *ntpPacket, std::size_t packetLen, std::size_t bufferLen);
	bool rescan(); // clears temp conent and parses the ntp packet again

	unsigned char *getNtpPacket() const;
	std::size_t getNtpPacketLength() const; // current
	std::size_t getBufferLength() const; // current


	NtpExtFieldStack & getExtFieldStack();
	std::vector<unsigned char> const & getMAC() const;
	std::vector<unsigned char> getSerializedPackage() const;


	std::size_t getFinalSize() const; // ntp packet length after 'overwriteNtpPacket()' operation
	bool overwriteNtpPacket(); // schreibt die neuen EFs in den Buffer


	bool isValid() const;
	bool isMacAvailabe() const;

	//only for debugging
	void printCurrentNtp() const; // aktuelles NTP-Paket ausgeben
	void printFinalNtp() const;   // aktualisiertes NTP-Paket (mit neuen Erweiterungsfeldern) ausgeben


private:

	bool parseNtpPacket(unsigned char *ntpPacket, std::size_t ntpPacketLen);

	static constexpr std::size_t c_ntpHeader = 48;
	static constexpr std::size_t c_extFieldHeader = 4;

	unsigned char *mp_ntpPacket;
	std::size_t m_ntpPacketLength;
	std::size_t m_bufferLength;

	NtpExtFieldStack m_extensionFields;
	std::vector<unsigned char> m_MAC;
	bool m_isValid;

};

