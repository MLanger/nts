# https://github.com/lefticus/cppbestpractices/blob/master/02-Use_the_Tools_Available.md
if(MSVC)
	string(REGEX REPLACE "/W3" "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})
	add_compile_options(/W4 /permissive- /w14640)
else()
	add_compile_options(-Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic)
endif()

add_subdirectory(core)
add_subdirectory(c_api)
add_subdirectory(cpp_api)
