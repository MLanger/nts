#include <stdio.h>
#include <stdlib.h>
#include "NtsUnicastService_C.h"
#include "NtpPacketInfo_C.h"


#ifdef _MSC_VER
//enable debug
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifdef _DEBUG
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif
#endif



int main()
{
#ifdef _MSC_VER
	// find memory leaks
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
//	_CrtSetBreakAlloc(25042);
#endif

	// pseudo NTP-Paket
	size_t bufLen = 48;
	int ntpPacketLen = 48;
	unsigned char *ntp = (unsigned char*)malloc(bufLen);
	for (unsigned int i = 0; i < bufLen; i++)
	{
		*(ntp + i) = (unsigned char)i;
	}


	NTS_CTX *ntsClient = NTS_CTX_new();
	NTS_init(ntsClient, "configClient.ini");
	NTS_show_infos(ntsClient);
	
	NTS_CTX *ntsServer = NTS_CTX_new();
	NTS_init(ntsServer, "configServer.ini");


	
	// ========================================================================================
	// CLIENT
	// ========================================================================================
	printf("\n\nCLIENT:\n-----------------\n");

	PACKET_INFO *pkifo = PACKET_INFO_new();
	PACKET_INFO_set_server(pkifo, "127.0.0.1", "127.0.0.2", 123);


	// NTS-Nachricht erstellen (request): INIT
	int result = NTS_create_message_init(ntsClient, pkifo);
	if (result < 0)
	{
		printf("[FAIL] 'NTS_create_message_init()':  error code: %d\n", result);
	}
	else
	{
		printf("[SUCCESS] 'NTS_create_message_init()': required space for NTS content: %d bytes\n", result);
		// extend ntp packet buffer
		if (result > 0)
		{
			bufLen += result;
			ntp = (unsigned char*)realloc(ntp, bufLen);
			printf("NTP packet resized! New buffer length: %llu bytes\n", bufLen);
		}
	}



	// NTS-Nachricht erstellen (request): UPDATE
	result = NTS_create_message_update(ntsClient, pkifo, ntp, bufLen, ntpPacketLen);
	if (result < 0)
	{
		printf("[FAIL] 'NTS_create_message_update()':  error code: %d\n", result);
	}
	else
	{
		printf("[SUCCESS] 'NTS_create_message_update()': current NTP packet size: %d bytes\n", result);
		ntpPacketLen = result;
	}



	// NTS-Nachricht erstellen (request): FINAL
	result = NTS_create_message_final(ntsClient, pkifo, ntp, bufLen, ntpPacketLen, false);
	if (result < 0)
	{
		printf("[FAIL] 'NTS_create_message_final()':  error code: %d\n", result);
	}
	else
	{
		printf("[SUCCESS] 'NTS_create_message_final()': current NTP packet size: %d bytes\n", result);
		ntpPacketLen = result;
	}


	// auslesen, an welchen NTP server das NTP gesendet werden soll
	// const char* ntp_dest_ip = PACKET_INFO_get_time_server_ip(pkifo);
	// unsigned short ntp_dest_port = PACKET_INFO_get_time_server_port(pkifo);

	// cleanup
	PACKET_INFO_free(pkifo);
	
	

	// ========================================================================================
	// SERVER
	// ========================================================================================
	printf("\n\nSERVER:\n-----------------\n");

	int ntsSpaceAvailable = 0;
	pkifo = PACKET_INFO_new();

	// NTS-Nachricht überprüfen und parsen
	result = NTS_process_and_verify(ntsServer, pkifo, ntp, bufLen, ntpPacketLen);

	if (result < 0)
	{
		printf("[FAIL] 'NTS_process_and_verify()':  error code: %d\n", result);
	}
	else
	{
		printf("[SUCCESS] 'NTS_process_and_verify()': current NTS content size: %d bytes\n", result);
		ntsSpaceAvailable = result;
	}



	// NTS-Nachricht erstellen (response): INIT
	result = NTS_create_message_init(ntsServer, pkifo);
	if (result < 0)
	{
		printf("[FAIL] 'NTS_create_message_init()':  error code: %d\n", result);
	}
	else
	{
		printf("[SUCCESS] 'NTS_create_message_init()': required space for NTS content: %d bytes\n", result);

		// extend ntp packet buffer (if necessary) 
		if ((result - ntsSpaceAvailable) > 0)
		{
			bufLen += (result - ntsSpaceAvailable);
			ntp = (unsigned char*)realloc(ntp, bufLen);
			printf("NTP packet resized! New buffer length: %llu bytes\n", bufLen);
		}
	}



	// NTS-Nachricht erstellen (request): UPDATE
	result = NTS_create_message_update(ntsServer, pkifo, ntp, bufLen, ntpPacketLen);
	if (result < 0)
	{
		printf("[FAIL] 'NTS_create_message_update()':  error code: %d\n", result);
	}
	else
	{
		printf("[SUCCESS] 'NTS_create_message_update()': current NTP packet size: %d bytes\n", result);
		ntpPacketLen = result;
	}



	// NTS-Nachricht erstellen (request): FINAL
	result = NTS_create_message_final(ntsServer, pkifo, ntp, bufLen, ntpPacketLen, false);
	if (result < 0)
	{
		printf("[FAIL] 'NTS_create_message_final()':  error code: %d\n", result);
	}
	else
	{
		printf("[SUCCESS] 'NTS_create_message_final()': current NTP packet size: %d bytes\n", result);
		ntpPacketLen = result;
	}


	// cleanup
	PACKET_INFO_free(pkifo);




	// ========================================================================================
	// CLIENT
	// ========================================================================================

	printf("\n\nCLIENT:\n-----------------\n");
	pkifo = PACKET_INFO_new();
	PACKET_INFO_set_server(pkifo, "", "127.0.0.2", 0); // IP-Adresse des NTP Servers (um die NTS-Nachricht zuzuordnen)


	// NTS-Nachricht überprüfen und parsen
	result = NTS_process_and_verify(ntsClient, pkifo, ntp, bufLen, ntpPacketLen);

	if (result < 0)
	{
		printf("[FAIL] 'NTS_process_and_verify()':  error code: %d\n", result);
	}
	else
	{
		printf("[SUCCESS] 'NTS_process_and_verify()': current NTS content size: %d bytes\n", result);
		ntsSpaceAvailable = result;
	}


	PACKET_INFO_free(pkifo);
	
	NTS_CTX_free(ntsClient);
	NTS_CTX_free(ntsServer);

	free(ntp);


	printf("\n\nENDE\n");
	getchar();
	return 0;
}
